﻿#include <iostream>
#include <vector>
#include <unordered_map>
#include <queue>
#include <stack>
#include <windows.h>
#include <functional>

using namespace std;

struct City {
	string name;
	int index;
	City(string n, int i) : name(n), index(i) {}
};

vector<City> cities;

void printRouteAndDistance(const vector<string>& route, int distance) {
	cout << "Маршрут: ";
	for (int i = 0; i < route.size() - 1; i++) {
		cout << route[i] << " -> ";
	}
	cout << route.back() << ", Дистанція: " << distance << " км" << endl;
}

vector<vector<string>> bfs(const vector<vector<int>>& graph, int start) {
	int n = graph.size(); // Отримання кількості вершин у графі
	vector<bool> visited(n, false); // Вектор для зберігання використаних вершин
	vector<int> parent(n, -1); // Вектор для зберігання батьківських вершин
	queue<int> q; // Черга для обходу графа
	vector<vector<string>> routes(n); // Вектор для зберігання маршрутів

	visited[start] = true; // Позначення початкової вершини як відвіданої
	q.push(start); // Додавання початкової вершини до черги

	while (!q.empty()) { // Поки черга не пуста
		int u = q.front(); // Отримання вершини з початку черги
		q.pop(); // Видалення вершини з початку черги

		for (int v = 0; v < n; v++) { // Перебір всіх вершин
			if (graph[u][v] != -1 && !visited[v]) { // Якщо вершина v суміжна з вершиною u та ще не відвідана
				visited[v] = true; // Позначення вершини v як відвіданої
				parent[v] = u; // Збереження батьківської вершини
				q.push(v); // Додавання вершини v до черги

				vector<string> route; // Вектор для зберігання маршруту
				int current = v; // Початкова вершина
				while (current != -1) { // Поки не дійшли до початкової вершини
					route.push_back(cities[current].name); // Додавання назви вершини до маршруту
					current = parent[current]; // Перехід до батьківської вершини
				} 
				reverse(route.begin(), route.end()); 
				routes[v] = route;
			}
		}
	}

	return routes;
}

vector<vector<string>> dfs(const vector<vector<int>>& graph, int start) {
	int n = graph.size(); // Отримання кількості вершин у графі
	vector<bool> visited(n, false); // Вектор для зберігання використаних вершин
	vector<int> parent(n, -1); // Вектор для зберігання батьківських вершин
	vector<vector<string>> routes(n); // Вектор для зберігання маршрутів

	// Рекурсивна функція для обходу графа
	function<void(int)> dfs_recursive = [&](int u) -> void {
		visited[u] = true; // Позначення вершини як відвіданої
		for (int v = 0; v < n; v++) { // Перебір всіх вершин
			if (graph[u][v] != -1 && !visited[v]) { // Якщо вершина v суміжна з вершиною u та ще не відвідана
				parent[v] = u; // Збереження батьківської вершини
				dfs_recursive(v); // Рекурсивний виклик для вершини v

				vector<string> route; // Вектор для зберігання маршруту
				int current = v; // Початкова вершина
				while (current != -1) { // Поки не дійшли до початкової вершини
					route.push_back(cities[current].name); // Додавання назви вершини до маршруту
					current = parent[current]; // Перехід до батьківської вершини
				}
				reverse(route.begin(), route.end());
				routes[v] = route;
			}
		}
	};

	dfs_recursive(start);

	return routes;
}

int main() {
	SetConsoleOutputCP(1251);
	SetConsoleCP(1251);

	unordered_map<string, int> city_index;
	int index = 0;
	city_index["Київ"] = index;
	cities.emplace_back("Київ", index++);
	city_index["Житомир"] = index;
	cities.emplace_back("Житомир", index++);
	city_index["Новоград-Волинський"] = index;
	cities.emplace_back("Новоград-Волинський", index++);
	city_index["Рівно"] = index;
	cities.emplace_back("Рівно", index++);
	city_index["Луцьк"] = index;
	cities.emplace_back("Луцьк", index++);
	city_index["Бердичів"] = index;
	cities.emplace_back("Бердичів", index++);
	city_index["Вінниця"] = index;
	cities.emplace_back("Вінниця", index++);
	city_index["Хмельницький"] = index;
	cities.emplace_back("Хмельницький", index++);
	city_index["Тернопіль"] = index;
	cities.emplace_back("Тернопіль", index++);
	city_index["Шепетівка"] = index;
	cities.emplace_back("Шепетівка", index++);
	city_index["Біла церква"] = index;
	cities.emplace_back("Біла церква", index++);
	city_index["Умань"] = index;
	cities.emplace_back("Умань", index++);
	city_index["Черкаси"] = index;
	cities.emplace_back("Черкаси", index++);
	city_index["Кременчук"] = index;
	cities.emplace_back("Кременчук", index++);
	city_index["Полтава"] = index;
	cities.emplace_back("Полтава", index++);
	city_index["Харків"] = index;
	cities.emplace_back("Харків", index++);
	city_index["Прилуки"] = index;
	cities.emplace_back("Прилуки", index++);
	city_index["Суми"] = index;
	cities.emplace_back("Суми", index++);

	int n = cities.size();
	vector<vector<int>> graph(n, vector<int>(n, -1));

	graph[city_index["Київ"]][city_index["Житомир"]] = 135;
	graph[city_index["Житомир"]][city_index["Новоград-Волинський"]] = 80;
	graph[city_index["Новоград-Волинський"]][city_index["Рівно"]] = 100;
	graph[city_index["Рівно"]][city_index["Луцьк"]] = 68;
	graph[city_index["Житомир"]][city_index["Бердичів"]] = 38;
	graph[city_index["Бердичів"]][city_index["Вінниця"]] = 73;
	graph[city_index["Вінниця"]][city_index["Хмельницький"]] = 110;
	graph[city_index["Хмельницький"]][city_index["Тернопіль"]] = 104;
	graph[city_index["Житомир"]][city_index["Шепетівка"]] = 115;
	graph[city_index["Київ"]][city_index["Біла церква"]] = 78;
	graph[city_index["Біла церква"]][city_index["Умань"]] = 115;
	graph[city_index["Біла церква"]][city_index["Черкаси"]] = 146;
	graph[city_index["Черкаси"]][city_index["Кременчук"]] = 105;
	graph[city_index["Біла церква"]][city_index["Полтава"]] = 181;
	graph[city_index["Полтава"]][city_index["Харків"]] = 130;
	graph[city_index["Київ"]][city_index["Прилуки"]] = 128;
	graph[city_index["Прилуки"]][city_index["Суми"]] = 175;

	int start_index = city_index["Київ"];

	cout << "Маршрути BFS від Києва до інших міст:" << endl;
	vector<vector<string>> bfs_routes = bfs(graph, start_index);
	for (int i = 0; i < n; i++) {
		if (i != start_index) {
			cout << "Київ -> " << cities[i].name << ": ";
			if (bfs_routes[i].empty()) {
				cout << "Немає маршруту" << endl;
			}
			else {
				int current_city = cities[i].index;
				int distance = 0;
				while (graph[city_index["Київ"]][current_city] == -1) {
					bool found = false;
					for (int i = 0; i < n; i++) {
						if (graph[i][current_city] != -1) {
							distance += graph[i][current_city];
							current_city = i;
							found = true;
							break;
						}
					}
					if (!found) {
						break;
					}
				}
				printRouteAndDistance(bfs_routes[i], distance);
			}
		}
	}

	cout << "Маршрути DFS від Києва до інших міст:" << endl;
	vector<vector<string>> dfs_routes = dfs(graph, start_index);
	for (int i = 0; i < n; i++) {
		if (i != start_index) {
			cout << "Київ -> " << cities[i].name << ": ";
			if (dfs_routes[i].empty()) {
				cout << "Немає маршруту" << endl;
			}
			else {
				int current_city = cities[i].index;
				int distance = 0;
				while (graph[city_index["Київ"]][current_city] == -1) {
					bool found = false;
					for (int i = 0; i < n; i++) {
						if (graph[i][current_city] != -1) {
							distance += graph[i][current_city];
							current_city = i;
							found = true;
							break;
						}
					}
					if (!found) {
						break;
					}
				}
				printRouteAndDistance(dfs_routes[i], distance);
			}
		}
	}

	return 0;
}